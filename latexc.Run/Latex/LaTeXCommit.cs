﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using LaTeXc.Run.Tasks;
using LaTeXc.Run.Utils;

namespace LaTeXc.Run.Latex
{
    public class LaTeXCommit : Task
    {
        private const int BYTES_TO_READ = sizeof(long);
        private LaTeX _latexTask;
        private string _outputPath;

        public LaTeXCommit(Database db, LaTeX taskLatex, string output) : base(db, "latex_commit::" + NormalizeInputPath(taskLatex.FileName))
        {
            _latexTask = taskLatex;
            _outputPath = output;
            Status = "There were errors";
        }

        public string Status { get; private set; }

        private static object InputLatex(Task self1, params object[] _)
        {
            var self = (LaTeXCommit) self1;
            self.PrintVerbose($"Method '{nameof(LaTeXCommit)}::{nameof(InputLatex)}'");
            return (self._latexTask.Status, self._latexTask.OutName);
        }

        protected override RunResult Execute()
        {
            this.PrintVerbose($"Method '{nameof(LaTeXCommit)}::{nameof(Execute)}'");
            Status = "There were errors";
            var (status, outName) = Input<(int?, string)>(InputLatex);

            if (status != 0 || outName == null)
            {
                if (outName == null)
                {
                    Status = "No pages of output";
                }

                return new RunResult(new List<string>(), null);
            }

            var commit = _outputPath;
            if (commit == null)
            {
                commit = Path.GetFileName(outName);
                if (!Path.IsPathRooted(commit))
                {
                    commit = Path.Join(Path.GetDirectoryName(_latexTask.FileName), commit);
                }
            }
            if (commit == outName)
            {
                Status = null;
                return new RunResult(new List<string>(), null);
            }

            if (File.Exists(commit) && FilesAreEqual(new FileInfo(commit), new FileInfo(outName)))
            {
                File.SetLastWriteTime(outName, DateTime.Now);
            }
            else
            {
                File.Copy(outName, outName + "~");
                File.Move(outName + "~", commit, true);
            }

            Input(InputFile, outName);
            Status = null;

            return new RunResult(new List<string> {commit}, null);
        }

        private  bool FilesAreEqual(FileInfo first, FileInfo second)
        {
            this.PrintVerbose($"Method '{nameof(LaTeXCommit)}::{nameof(FilesAreEqual)}'");
            if (first.Length != second.Length)
                return false;

            if (string.Equals(first.FullName, second.FullName, StringComparison.OrdinalIgnoreCase))
                return true;

            var iterations = (int) Math.Ceiling((double) first.Length / BYTES_TO_READ);

            using (var fs1 = first.OpenRead())
            {
                using var fs2 = second.OpenRead();
                var one = new byte[BYTES_TO_READ];
                var two = new byte[BYTES_TO_READ];

                for (var i = 0; i < iterations; i++)
                {
                    fs1.Read(one, 0, BYTES_TO_READ);
                    fs2.Read(two, 0, BYTES_TO_READ);

                    if (BitConverter.ToInt64(one, 0) != BitConverter.ToInt64(two, 0))
                        return false;
                }
            }

            return true;
        }

        public override int Report()
        {
            this.PrintVerbose($"Method '{nameof(LaTeXCommit)}::{nameof(Report)}'");
            return Status == null ? 0 : 1;
        }
    }
}