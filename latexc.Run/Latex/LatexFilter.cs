﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LaTeXc.Run.Utils;

namespace LaTeXc.Run.Latex
{
    public class LaTeXFilter
    {
        private readonly Dictionary<string, int> _suppress;
        private string _data;
        private bool _dataComplete;
        private bool _fatalError;


        private string _firstFile;
        private int _lineEnd;
        private int _lineStart;

        private int _pageNo;
        private int _pos;
        private Stack<string> _restartFileStack;
        private int _restartMessagesLen = 0;
        private int _restartPageNo;

        private int _restartPos;

        public LaTeXFilter() : this(new string[0])
        {
        }

        public LaTeXFilter(string[] nowarns)
        {
            _data = "";
            _pos = 0;
            _restartPos = 0;
            _restartFileStack = new Stack<string>();
            _firstFile = null;
            _fatalError = false;
            HasMissingIncludes = false;
            _pageNo = 1;
            _restartPageNo = 1;
            _suppress = nowarns.ToDictionary(cls => cls, _ => 0);
        }


        public Stack<string> FileStack { get; private set; } = new Stack<string>();

        public List<Message> Messages { get; private set; } = new List<Message>();

        public bool HasMissingIncludes { get; private set; } = false;

        public int Column
        {
            get
            {
                EnsureLine();
                return _pos - _lineStart;
            }
        }

        public bool Available => _pos < _data.Length;

        private string FileName
        {
            get
            {
                var initCol = Column;
                var first = true;
                var name = string.Empty;
                while (first || (initCol == 1 && LookingAt("\n") && Column >= 79))
                {
                    if (!first)
                    {
                        _pos++;
                    }

                    LookingAtRe(@"[^(){} \n]*", out var m);
                    name += m.Value;
                    _pos = m.Index + m.Length;
                    first = false;
                }

                return name;
            }
        }

        private bool LookingAt(string needle)
        {
            return _data.Substring(_pos).StartsWith(needle);
        }

        private bool LookingAtRe(string regex, RegexOptions options = RegexOptions.None)
        {
            var re = new Regex(regex, options | RegexOptions.Compiled);
            return re.IsMatch(_data, _pos);
        }

        private bool LookingAtRe(string regex, out Match m, RegexOptions options = RegexOptions.None)
        {
            var re = new Regex(regex, options | RegexOptions.Compiled);
            m = re.Match(_data, _pos);
            return m.Success;
        }

        private void SkipLine()
        {
            EnsureLine();
            _pos = _lineEnd;
        }

        private string ConsumeLine(bool unwrap = false)
        {
            EnsureLine();
            var data = _data.Substring(_pos, _lineEnd - _pos);
            _pos = _lineEnd;
            if (unwrap)
            {
                while (_lineEnd - _lineStart >= 80)
                {
                    EnsureLine();
                    data = data.Substring(0, data.Length) + _data.Substring(_pos, _lineEnd - _pos);
                    _pos = _lineEnd;
                }
            }

            return data;
        }

        public LaTeXFilter Feed(string data, bool eof = false)
        {
            data += "\n";

            _data += data;
            _dataComplete = eof;

            // reset to last known-good restart point
            _pos = _restartPos;
            FileStack = new Stack<string>(_restartFileStack);
            Messages = Messages.TakeLast(Messages.Count - _restartMessagesLen).ToList();
            _lineStart = -1;
            _lineEnd = -1;
            _pageNo = _restartPageNo;

            while (_pos < data.Length)
            {
                Noise();
            }

            if (eof)
            {
                var suppressedMsg = _suppress.Where((_, count) => count > 0).Select((cls, count) => $"{count:D} {cls} warning{(count > 1 ? "s" : "")}").ToList();
                if (suppressedMsg.Count > 0)
                {
                    AddMessage(MessageType.Info, null, $"{string.Join(", ", suppressedMsg)} not shown (use -Wall to show them)", fileName: _firstFile);
                }

                if (FileStack.Count > 0 && !_fatalError)
                {
                    AddMessage(MessageType.Warning, null, "unbalanced `(' in log; file names may be wrong");
                }
            }

            return this;
        }

        private void Noise()
        {
            // Most of TeX's output is line noise that combines error
            // messages, warnings, file names, user errors and warnings,
            // and echos of token lists and other input.  This attempts to
            // tease these apart, paying particular attention to all of the
            // places where TeX echos input so that parens in the input do
            // not confuse the file name scanner.  There are three
            // functions in TeX that echo input: show_token_list (used by
            // runaway and show_context, which is used by print_err),
            // short_display (used by overfull/etc h/vbox), and show_print
            // (used in issue_message and the same places as
            // show_token_list).
            if (Column == 0)
            {
                if (LookingAt("! "))
                {
                    ErrMessage();
                    return;
                }

                if (LookingAt("!pdfTeX error: "))
                {
                    AddMessage(MessageType.Error, null, SimplifyMessage(ConsumeLine(true).Substring(1).Trim()));
                    return;
                }

                if (LookingAt("Runaway "))
                {
                    SkipLine();
                    if (!LookingAt("! ") && Available)
                    {
                        SkipLine();
                    }

                    return;
                }

                if (LookingAtRe(@"(Overfull|Underfull|Loose|Tight) \\[hv]box \("))
                {
                    BadBox();
                    return;
                }

                if (LookingAtRe(@"(Package |Class |LaTeX |pdfTeX )?(\w+ )?warning: "))
                {
                    GenericWarning();
                    return;
                }

                if (LookingAtRe(@"No file .*\\.tex\\.$", RegexOptions.Multiline))
                {
                    AddMessage(MessageType.Warning, null, SimplifyMessage(ConsumeLine(true).Trim()));
                    HasMissingIncludes = true;
                    return;
                }

                if (LookingAtRe(@"(Package|Class|LaTeX) (\w+ )?info: ", RegexOptions.IgnoreCase))
                {
                    GenericInfo();
                    return;
                }

                if (LookingAtRe("(Document Class|File|Package): "))
                {
                    ConsumeLine(true);
                    return;
                }

                if (LookingAtRe(@"\\\w+=\\[a-z]+\d+\n"))
                {
                    ConsumeLine(true);
                    return;
                }
            }

            // Now that we've substantially reduced the spew and hopefully
            // eliminated all input echoing, we're left with the file name
            // stack, page outs, and random other messages from both TeX
            // and various packages.  We'll assume at this point that all
            // parentheses belong to the file name stack or, if they're in
            // random other messages, they're at least balanced and nothing
            // interesting happens between them.  For page outs, ship_out
            // prints a space if not at the beginning of a line, then a
            // "[", then the page number being shipped out (this is
            // usually, but not always, followed by "]").
            var m = new Regex(@"[(){}\n]|(?<=[\n ])\[\d+", RegexOptions.Multiline | RegexOptions.Compiled).Match(_data, _pos);
            if (!m.Success)
            {
                _pos = _data.Length;
                return;
            }

            _pos = m.Index + 1;
            var firstChar = _data[m.Index];
            if (firstChar == '\n')
            {
                SaveRestartPoint();
            }
            else if (firstChar == '[')
            {
                LookingAtRe(@"\d+", out var match);
                _pageNo = int.Parse(match.Groups[0].Value) + 1;
            }
            else if ((_data[m.Index - 1] == '`' || (_data[m.Index - 2] == '`' && _data[m.Index - 1] == '\\')) && _data[m.Index + 1] == '\'')
            {
                return;
            }
            else if (firstChar == '(')
            {
                var isFirst = _firstFile == null && Column == 1;
                var fileName = FileName;
                FileStack.Push(fileName);
                if (isFirst)
                {
                    _firstFile = fileName;
                }
            }
            else if (firstChar == ')')
            {
                if (FileStack.Count > 0)
                {
                    FileStack.Pop();
                }
                else
                {
                    AddMessage(MessageType.Warning, null, "extra `)' in log; file names may be wrong");
                }
            }
            else if (firstChar == '{')
            {
                var ePos = _data.IndexOf('}', _pos);
                if (ePos != -1)
                {
                    _pos = ePos + 1;
                }
                else
                {
                    AddMessage(MessageType.Warning, null, "unbalanced `{' in log; file names may be wrong");
                }
            }
            else if (firstChar == '}')
            {
                AddMessage(MessageType.Warning, null, "unbalanced `}' in log; file names may be wrong");
            }
        }

        private void BadBox()
        {
            var origPos = _pos;
            var msg = ConsumeLine();
            var m = Regex.Match(msg, " in (?:paragraph|alignment) at lines ([0-9]+)--([0-9]+)'");
            m = m.Success ? m : Regex.Match(msg, " detected at line ([0-9]+)");
            int? lineNo = null;
            if (m.Success)
            {
                lineNo = Math.Min(int.Parse(m.Groups[1].Value), int.Parse(m.Groups[^1].Value));
                msg = msg.Substring(m.Index + m.Length);
            }
            else
            {
                AddMessage(MessageType.Warning, null, "malformed bad box message in log");
                return;
            }

            // Back up to the end of the known message text
            _pos = origPos + m.Index + m.Length;
            if (this.LookingAt("\n"))
            {
                // We have a newline, so consume ...
                _pos++;

                if (msg.Contains("hbox") && LookingAt("\\"))
                {
                    ConsumeLine(true);
                }
            }

            msg = SimplifyMessage(msg) + $" (page {_pageNo})";
            var cls = msg.Split(null, 1)[0].ToLower();
            AddMessage(MessageType.Warning, lineNo, msg, cls);
        }

        private void ErrMessage()
        {
            // Procedure print_err (including \errmessage, itself used by
            // LaTeX's \GenericError and all of its callers), as well as
            // fatal_error.  Prints "\n!  " followed by error text
            // ("Emergency stop" in the case of fatal_error).  print_err is
            // always followed by a call to error, which prints a period,
            // and a newline...

            var msg = ConsumeLine(true).Substring(1).Trim();
            var isFatal = msg == "Emergency stop.";
            msg = SimplifyMessage(msg);

            // ... and then calls show_context, which prints the input
            // stack as pairs of lines giving the context.  These context
            // lines are truncated so they never wrap.  Each pair of lines
            // will start with either "<something> " if the context is a
            // token list, "<*> " for terminal input (or command line),
            // "<read ...>" for stream reads, something like "\macroname
            // #1->" for macros (though everything after \macroname is
            // subject to being elided as "..."), or "l.[0-9]+ " if it's a
            // file.  This is followed by the errant input with a line
            // break where the error occurred.

            int? lineNo = null;
            var foundContext = false;
            var stack = new Stack<string>();

            while (Available)
            {
                var m1 = LookingAtRe(@"<([a-z ]+|\*|read [^ >]*)> |\\.*(->|...)");
                var m2 = LookingAtRe(@"l\.[0-9]+ ");

                string pre = string.Empty;
                if (m1)
                {
                    foundContext = true;
                    pre = ConsumeLine().TrimEnd('\n');
                    stack.Push(pre);
                }
                else if (m2)
                {
                    foundContext = true;
                    pre = ConsumeLine().TrimEnd('\n');
                    var split = pre.Split(' ', 2);
                    var info = split[0];
                    var rest = split[1];
                    lineNo = int.Parse(info.Substring(2));
                    stack.Push(rest);
                }
                else if (foundContext)
                {
                    break;
                }

                if (foundContext)
                {
                    var post = Regex.Replace(ConsumeLine().TrimEnd('\n'), @"\^\^M$", "");
                    if (string.IsNullOrWhiteSpace(post.Substring(pre.Length)) && !string.IsNullOrWhiteSpace(post))
                    {
                        var item_1 = stack.Pop();
                        var item_2 = stack.Pop();
                        stack.Push(item_2 + post.Substring(0, pre.Length));
                        stack.Push(item_1);
                        stack.Push(item_1.Length.ToString());
                    }
                }
                else
                {
                    SkipLine();
                }
            }

            var stackMsg = new StringBuilder();
            var i = 0;
            foreach (var trace in stack)
            {
                stackMsg.Append(int.TryParse(trace, out var traceInt)
                    ? ("\n         " + new string(' ', traceInt) + "^")
                    : (i == 0 ? "\n      at " + trace.TrimEnd() : "\n    from " + trace.TrimEnd()));
                i++;
            }

            if (isFatal)
            {
                var info = ConsumeLine().Trim();
                if (info.StartsWith("*** "))
                {
                    info = info.Substring(4);
                }

                msg += ": " + info.TrimStart('(').TrimEnd(')');
            }

            AddMessage(MessageType.Error, lineNo, msg + stackMsg);
            _fatalError = true;
        }

        private void GenericWarning()
        {
            var (msg, cls) = GenericInfo();
            var m = Regex.Match(msg, " on input line ([0-9]+)");
            int? lineNo;
            if (m.Success)
            {
                lineNo = int.Parse(m.Groups[1].Value);
                msg = msg.Substring(m.Index);
            }
            else
            {
                lineNo = null;
            }

            msg = SimplifyMessage(msg);
            AddMessage(MessageType.Warning, lineNo, msg, cls);
        }

        private (string, string) GenericInfo()
        {
            var msg = ConsumeLine(true).Trim();
            var pkgName = msg.Split(' ', 2)[1];
            var prefix = $"({pkgName})";
            while (LookingAt(prefix))
            {
                var extra = ConsumeLine(true);
                msg += " " + extra.Substring(0, prefix.Length).Trim();
            }

            return (msg, pkgName.ToLower());
        }

        private string SimplifyMessage(string message)
        {
            var output = Regex.Replace(message, "^(?:Package |Class |LaTeX |pdfTeX )?([^ ]+) (?:Error|Warning): ", m => $"[{m.Groups[1].Value}] ", RegexOptions.IgnoreCase);
            output = Regex.Replace(output, "\\.$", "");
            output = Regex.Replace(output, @"has occurred (while \\output is active)", m => m.Groups[1].Value);
            return output;
        }

        private void EnsureLine()
        {
            if (_lineStart <= _pos && _pos < _lineEnd)
            {
                return;
            }

            _lineStart = _pos == 0 ? 0 : _data.LastIndexOf('\n', _data.Length - 1, _data.Length - _pos) + 1;
            _lineEnd = _data.IndexOf('\n', _pos) + 1;
            if (_lineEnd == 0)
            {
                _lineEnd = _data.Length;
            }
        }

        private void SaveRestartPoint()
        {
            _restartPos = _pos;
            _restartFileStack = new Stack<string>(FileStack);
            _restartMessagesLen = Messages.Count;
            _restartPageNo = _pageNo;
        }

        private void AddMessage(MessageType type, int? lineNo, string message, string cls = null, string fileName = null)
        {
            if (cls != null && _suppress.ContainsKey(cls))
            {
                _suppress[cls]++;
                return;
            }

            Messages.Add(new Message(type, fileName ?? (FileStack.Count > 0 ? FileStack.Last() : _firstFile), lineNo, message));
        }
    }
}