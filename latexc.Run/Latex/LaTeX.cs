﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using LaTeXc.Run.Tasks;
using LaTeXc.Run.Utils;

namespace LaTeXc.Run.Latex
{
    public class LaTeX : Task
    {
        public LaTeX(Database db, string fileName, string latexCmd, string[] latexArgs, string objDir, string[] noWarns) : base(db, "latex::" + NormalizeInputPath(fileName))
        {
            FileName = fileName;
            Cwd = Path.GetDirectoryName(FileName);
            Cmd = latexCmd;
            CmdArgs = latexArgs;
            ObjDir = new DirectoryInfo(objDir).FullName;
            Nowarns = noWarns;
            Pass = 0;
        }

        public string FileName { get; }
        public string Cmd { get; }
        public string[] CmdArgs { get; }
        public string ObjDir { get; }
        public string[] Nowarns { get; }
        private int Pass { get; set; }
        public string Cwd { get; }

        private new LatexRunExtra ResultExtra => (LatexRunExtra) base.ResultExtra;

        public string JobName => ResultExtra?.JobName;
        public string OutName => ResultExtra?.OutName;
        public int? Status => ResultExtra?.Status;

        protected static object InputArgs(Task self1, params object[] _)
        {
            var self = (LaTeX) self1;
            self.PrintVerbose($"Method '{nameof(LaTeX)}::{nameof(InputArgs)}'");
            var fileName = self.FileName;
            if (fileName.StartsWith("-") || fileName.StartsWith("&") || fileName.StartsWith("\\"))
            {
                fileName = "./" + fileName;
            }

            self.PrintVerbose($"Local fileName is '{fileName}'");

            var cmd = new List<string>(1 + self.CmdArgs.Length + 6);
            cmd.Add(self.Cmd);
            cmd.AddRange(self.CmdArgs);
            cmd.AddRange(new[]
            {
                "-interaction",
                "nonstopmode",
                "-recorder",
                "-output-directory",
                "\"" + self.ObjDir + "\"",
                "\"" + fileName + "\""
            });

            self.PrintVerbose($"LaTeX command is '{string.Join(' ', cmd)}'");

            return cmd.ToArray();
        }

        protected override RunResult Execute()
        {
            this.PrintVerbose($"Method '{nameof(LaTeX)}::{nameof(Execute)}'");
            Pass++;
            var args = (string[]) Input(InputArgs);
            if (!Directory.Exists(ObjDir))
            {
                Directory.CreateDirectory(ObjDir);
            }

            var proc = new Process
            {
                EnableRaisingEvents = true,
                StartInfo =
                {
                    FileName = args[0],
                    Arguments = string.Join(' ', args).Substring(args[0].Length + 1),
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    WorkingDirectory = Cwd
                }
            };


            var prefix = "latex";
            if (Pass > 1)
            {
                prefix += $"({Pass})";
            }

            var logBuffer = new List<string>();
            var filter = new LaTeXFilter();

            string jobName = null;
            string outName = null;

            proc.OutputDataReceived += (sender, e) => this.PrintDebug("LATEX OUTPUT: " + e.Data);
            proc.ErrorDataReceived += (sender, e) => this.PrintDebug("LATEX ERROR: " + e.Data);
            proc.OutputDataReceived += (sender, e) =>
            {
                var data = e.Data;
                logBuffer.Add(data);
                // Filter
                filter.Feed(data);
                var fileStack = filter.FileStack;
                if (fileStack == null || fileStack.Count == 0) // TODO
                {
                    return;
                }

                /*var tos = fileStack.Last();
                if (tos.StartsWith("./"))
                {
                    tos = tos.Substring(2);
                }*/

                // TODO Update
            };

            proc.Start();
            proc.BeginOutputReadLine();
            proc.WaitForExit();

            var hasErrors = filter.Messages.Any(m => m.Type == MessageType.Error);
            var missingIncludes = filter.HasMissingIncludes;

            Input(InputEnv, "TEXMFOUTPUT");
            Input(InputEnv, "TEXINPUTS");
            Input(InputEnv, "TEXFORMATS");
            Input(InputEnv, "TEXPOOL");
            Input(InputEnv, "TFMFONTS");
            Input(InputEnv, "PATH");
            var fullLog = string.Join('\n', logBuffer);

            {
                var m = Regex.Match(fullLog, "^Transcript written on \"?(.*)\\.log\"?\\.$", RegexOptions.Multiline);
                if (!m.Success)
                {
                    throw new ApplicationException("failed to extract job name from latex log");
                }

                jobName = m.Groups[1].Value.Replace("\n", "");
            }

            {
                var m = Regex.Match(fullLog, "^Output written on \"?(.*\\.[^ .\"]+)\"? \\([0-9]+ page", RegexOptions.Multiline);
                if (m.Success)
                {
                    outName = m.Groups[1].Value.Replace("\n", "");
                }
                else
                {
                    if (Regex.IsMatch(fullLog, "^No pages of output\\.$|^! Emergency stop\\.$|^!  ==> Fatal error occurred, no output PDF file produced!$", RegexOptions.Multiline))
                    {
                        throw new ApplicationException("failed to extract output name from latex log");
                    }
                }
            }

            //LuaTeX
            if (Path.GetFileName(jobName) == jobName && File.Exists(Path.Join(ObjDir, jobName + ".log")))
            {
                jobName = Path.Join(ObjDir, jobName);
                if (outName != null)
                {
                    outName = Path.Join(ObjDir, outName);
                }
            }

            ParseRecorder(jobName, out var inputs, out var outputs);

            foreach (var output in outputs.Where(File.Exists))
            {
                Db.HashCache.Clobber(output);

                if (output.EndsWith(".bib"))
                {
                    var fileNameNoExt = Path.GetFileNameWithoutExtension(output);
                    var contents = File.ReadAllLines(jobName + ".aux").Select(line =>
                    {
                        var m = Regex.Match(line.Trim(), @"^\\bibdata\{([^}]+)\}$");
                        if (!m.Success)
                        {
                            return line;
                        }

                        return @"\bibdata{" + string.Join(',', m.Groups[1].Value.Split(',').Select(entry =>
                        {
                            var newName = Path.GetRelativePath(Cwd, output);
                            newName = newName.Substring(0, newName.LastIndexOf(".", StringComparison.Ordinal));
                            return entry.Replace(fileNameNoExt, newName);
                        })) + "}";
                    }).ToArray();
                    File.WriteAllLines(jobName + ".aux", contents);
                }
            }

            foreach (var path in inputs)
            {
                Input(InputFile, path);
            }

            // TODO: Missing includes

            if (!CreateOutDirs(fullLog) && hasErrors)
            {
                Runner.TerminateTaskLoop = true;
            }

            return new RunResult(outputs.Distinct().ToList(), new LatexRunExtra(jobName, outName, proc.ExitCode));
        }

        private bool CreateOutDirs(string fullLog)
        {
            this.PrintVerbose($"Method '{nameof(LaTeX)}::{nameof(CreateOutDirs)}'");
            var m = Regex.Match(fullLog, "^! I can\'t write on file `(.*)\'\\.$", RegexOptions.Multiline);
            if (m.Success && m.Groups[1].Value.Contains("/") && !m.Groups[1].Value.Contains("../"))
            {
                var subDir = Path.GetDirectoryName(m.Groups[1].Value);
                var newDir = Path.Join(ObjDir, subDir);
                if (Directory.Exists(subDir) && !Directory.Exists(newDir) && !File.Exists(newDir))
                {
                    Directory.CreateDirectory(newDir);
                    Input(InputUnstable);
                    return true;
                }
            }

            return false;
        }

        private void ParseRecorder(string jobName, out HashSet<string> inputs, out List<string> outputs)
        {
            this.PrintVerbose($"Method '{nameof(LaTeX)}::{nameof(ParseRecorder)}'");
            var fileName = jobName + ".fls";
            inputs = new HashSet<string>();
            outputs = new List<string>();
            var pwd = string.Empty;
            var text = File.ReadAllLines(fileName);
            var lineNum = 0;
            foreach (var line in text)
            {
                var parts = line.TrimEnd('\n').Split(' ', 2);
                switch (parts[0])
                {
                    case "PWD":
                        pwd = parts[1];
                        break;
                    case "INPUT":
                        inputs.Add(Path.IsPathRooted(parts[1]) ? parts[1] : Path.GetRelativePath(Environment.CurrentDirectory, Path.Join(pwd, parts[1])));
                        break;
                    case "OUTPUT":
                        outputs.Add(Path.IsPathRooted(parts[1]) ? parts[1] : Path.GetRelativePath(Environment.CurrentDirectory, Path.Join(pwd, parts[1])));
                        break;
                    default: throw new ApplicationException($"syntax error on line {lineNum} of {fileName}");
                }

                lineNum++;
            }

            outputs.Add(fileName);
        }

        public override int Report()
        {
            this.PrintVerbose($"Method '{nameof(LaTeX)}::{nameof(Report)}'");
            var extra = ResultExtra;

            if (extra == null)
            {
                return 0;
            }

            var logfile = File.ReadAllText(extra.JobName + ".log");
            var messages = new LaTeXFilter(Nowarns).Feed(logfile, true).Messages; //CleanMessages(new LaTeXFilter(Nowarns).Feed(logfile, true).Messages);
            foreach (var message in messages)
            {
                message.Emit();
            }

            return extra.Status;
        }

        private IEnumerable<Message> CleanMessages(IEnumerable<Message> messages)
        {
            this.PrintVerbose($"Method '{nameof(LaTeX)}::{nameof(CleanMessages)}'");
            var haveUndefinedReference = false;
            foreach (var message in messages)
            {
                if (message.Content == "==> Fatal error occurred, no output PDF file produced!")
                {
                    yield return new Message(MessageType.Error, null, null, "Fatal error (no output file produced)");
                }
                else if (message.Content.StartsWith("[LaTeX] "))
                {
                    var copy = new Message(message.Type, message.FileName, message.LineNo, message.Content.Split(' ', 2)[1]);
                    yield return copy;
                }
                else if (Regex.IsMatch(message.Content, "Reference .* undefined"))
                {
                    haveUndefinedReference = true;
                    yield return message;
                }
                else if (haveUndefinedReference && Regex.IsMatch(message.Content, "There were undefined references"))
                {
                    continue;
                }
            }
        }
    }
}