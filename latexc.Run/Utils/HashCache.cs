﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace LaTeXc.Run.Utils
{
    public class HashCache
    {
        private readonly Dictionary<string, string> _cache = new Dictionary<string, string>();

        public string Get(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return null;
            }

            if (_cache.TryGetValue(fileName, out var hash))
            {
                return hash;
            }

            using var fs = File.OpenRead(fileName);
            var h = new SHA256CryptoServiceProvider();
            var result = Convert.ToBase64String(h.ComputeHash(fs));
            _cache[fileName] = result;
            return result;
        }

        public void Clobber(string fileName)
        {
            if (_cache.ContainsKey(fileName))
            {
                return;
            }

            _cache[fileName] = "*";
        }

        public void Invalidate(string fileName)
        {
            if (_cache.ContainsKey(fileName))
            {
                _cache.Remove(fileName);
            }
        }
    }
}