﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using LaTeXc.Run.BibTeX;
using LaTeXc.Run.Latex;
using LaTeXc.Run.Tasks;

namespace LaTeXc.Run.Utils
{
    public static class EnvHelper
    {
        public static string Get(string varName)
        {
            if (varName == null)
            {
                throw new ArgumentNullException(nameof(varName));
            }

            return Environment.GetEnvironmentVariable(varName, EnvironmentVariableTarget.Process) ??
                   Environment.GetEnvironmentVariable(varName, EnvironmentVariableTarget.User) ??
                   Environment.GetEnvironmentVariable(varName, EnvironmentVariableTarget.Machine);
        }
    }

    internal static class PrintExtensions
    {
        internal static void PrintError(this object _, string message)
        {
            PrintError(message);
        }

        internal static void PrintWarning(this object _, string message)
        {
            PrintWarning(message);
        }

        internal static void PrintInfo(this object _, string message)
        {
            PrintInfo(message);
        }

        internal static void PrintDebug(this object _, string message)
        {
            PrintDebug(message);
        }

        internal static void PrintVerbose(this object _, string message)
        {
            PrintVerbose(message);
        }

        internal static void PrintError(string message)
        {
            STDERR.WriteLine("[  ERROR  ] " + message);
        }

        internal static void PrintWarning(string message)
        {
            STDOUT.WriteLine("[ WARNING ] " + message);
        }

        internal static void PrintInfo(string message)
        {
            STDOUT.WriteLine("[  INFO   ] " + message);
        }

        internal static void PrintDebug(string message)
        {
            STDDBG.WriteLine("[  DEBUG  ] " + message);
        }

        internal static void PrintVerbose(string message)
        {
            STDVER.WriteLine("[ VERBOSE ] " + message);
        }

        // ReSharper disable InconsistentNaming
        internal static TextWriter STDOUT = TextWriter.Null;
        internal static TextWriter STDERR = TextWriter.Null;
        internal static TextWriter STDDBG = TextWriter.Null;

        internal static TextWriter STDVER = TextWriter.Null;
        // ReSharper restore InconsistentNaming
    }

    public static class Runner
    {
        public const string CLEAN_ALL = "clean_all";
        public const string OBJ_DIR = "obj_dir";
        public const string FILE = "file";
        public const string OUTPUT = "output";
        public const string NOWARNS = "nowarns";
        public const string LATEX_ARGS = "latex_args";
        public const string LATEX_CMD = "latex_cmd";
        public const string BIBTEX_CMD = "bibtex_cmd";
        public const string BIBTEX_ARGS = "bibtex_args";
        public const string MAX_ITERATIONS = "max_iterations";

        public static bool TerminateTaskLoop = false;

#pragma warning disable 00028
        public static int Main(Dictionary<string, object> args, TextWriter stdout = null, TextWriter stderr = null)
        {
            var status = 0;
            Database db = null;
            string dbPath = null;
            try
            {
                if (stdout != null)
                {
                    PrintExtensions.STDOUT = stdout;
                }

                if (stderr != null)
                {
                    PrintExtensions.STDERR = stderr;
                }

                if (!args.ContainsKey(FILE) && !args.ContainsKey(CLEAN_ALL))
                {
                    PrintExtensions.PrintError("You need to define an action (either TeX file or --clean-all");
                    return 1;
                }

                if (args.ContainsKey("debug") && (bool) args["debug"])
                {
                    PrintExtensions.STDDBG = PrintExtensions.STDOUT;
                    PrintExtensions.PrintDebug("Debug enabled");
                }

                if (args.ContainsKey("verbose") && (bool) args["verbose"])
                {
                    PrintExtensions.STDVER = PrintExtensions.STDOUT;
                    PrintExtensions.PrintVerbose("Verbose enabled");
                }


                if (!args.ContainsKey(CLEAN_ALL))
                {
                    args[CLEAN_ALL] = false;
                    args.PrintDebug($"{CLEAN_ALL} not present, defaulting to {args[CLEAN_ALL]}");
                }

                if (!args.ContainsKey(OUTPUT))
                {
                    args[OUTPUT] = null;
                    PrintExtensions.PrintDebug($"{OUTPUT} not present, defaulting to {args[OUTPUT]}");
                }

                if (!args.ContainsKey(LATEX_CMD))
                {
                    args[LATEX_CMD] = "pdflatex";
                    PrintExtensions.PrintDebug($"{LATEX_CMD} not present, defaulting to {args[LATEX_CMD]}");
                }

                if (!args.ContainsKey(BIBTEX_CMD))
                {
                    args[BIBTEX_CMD] = "bibtex";
                    PrintExtensions.PrintDebug($"{BIBTEX_CMD} not present, defaulting to {args[BIBTEX_CMD]}");
                }

                if (!args.ContainsKey(LATEX_ARGS))
                {
                    args[LATEX_ARGS] = new string[0];
                    PrintExtensions.PrintDebug($"{LATEX_ARGS} not present, defaulting to {args[LATEX_ARGS]}");
                }

                if (!args.ContainsKey(BIBTEX_ARGS))
                {
                    args[BIBTEX_ARGS] = new string[0];
                    PrintExtensions.PrintDebug($"{BIBTEX_ARGS} not present, defaulting to {args[BIBTEX_ARGS]}");
                }

                if (!args.ContainsKey(NOWARNS))
                {
                    args[NOWARNS] = new[] {"underfull"};
                    PrintExtensions.PrintDebug($"{NOWARNS} not present, defaulting to {args[NOWARNS]}");
                }

                if (!args.ContainsKey(OBJ_DIR))
                {
                    args[OBJ_DIR] = Path.Join(Path.GetDirectoryName((string) args[FILE]), "latex.out");
                    PrintExtensions.PrintDebug($"{OBJ_DIR} not present, defaulting to {args[OBJ_DIR]}");
                }

                if (!args.ContainsKey(MAX_ITERATIONS))
                {
                    args[MAX_ITERATIONS] = 10;
                    PrintExtensions.PrintDebug($"{MAX_ITERATIONS} not present, defaulting to {args[MAX_ITERATIONS]}");
                }

                dbPath = Path.Join(new DirectoryInfo((string) args[OBJ_DIR]).FullName, ".latexrun.db");
                db = new Database(dbPath);

                if ((bool) args[CLEAN_ALL])
                {
                    db.DoClean((string) args[OBJ_DIR]);
                }

                if (!args.ContainsKey(FILE))
                {
                    return 0;
                }

                if (!Path.IsPathRooted((string) args[FILE]))
                {
                    args[FILE] = Path.Join(Environment.CurrentDirectory, (string) args[FILE]);
                }

                var taskLatex = new LaTeX(
                    db,
                    (string) args[FILE],
                    (string) args[LATEX_CMD],
                    (string[]) args[LATEX_ARGS],
                    (string) args[OBJ_DIR],
                    (string[]) args[NOWARNS]
                );
                var taskCommit = new LaTeXCommit(
                    db,
                    taskLatex,
                    (string) args[OUTPUT]
                );
                var taskBibtex = new BibTeX.BibTeX(
                    db,
                    taskLatex,
                    (string) args[BIBTEX_CMD],
                    (string[]) args[BIBTEX_ARGS],
                    (string[]) args[NOWARNS],
                    (string) args[OBJ_DIR]
                );

                var tasks = new List<Task> {taskLatex, taskCommit, taskBibtex};
                var stable = RunTasks(tasks, (int) args[MAX_ITERATIONS]);

                foreach (var task in tasks)
                {
                    status = Math.Max(task.Report(), status);
                }

                if (!stable)
                {
                    status = Math.Max(status, 1);
                }
            }
            catch (Exception e)
            {
                stderr.WriteLine("[ ERROR ] " + e.Message);
                stderr.WriteLine("[ ERROR ] " + e.StackTrace.Replace("\n", "\n[ ERROR ] "));
                status = 1;
            }
            finally
            {
                try
                {
                    File.Delete(dbPath + ".lock");
                }
                catch (IOException)
                {
                }
            }

            return status;
        }

        public static bool RunTasks(List<Task> tasks, int maxIterations)
        {
            PrintExtensions.PrintVerbose($"Method '{nameof(Runner)}::{nameof(RunTasks)}'");
            PrintExtensions.PrintInfo($"Running {tasks.Count} task(s) for maximum {maxIterations} time(s)");
            var nStable = 0;
            for (var i = 0; i < maxIterations; i++)
            {
                PrintExtensions.PrintDebug($"Entering task loop #{i}");
                foreach (var task in tasks)
                {
                    if (task.Stable)
                    {
                        PrintExtensions.PrintVerbose($"Task {task.TaskId} is stable");
                        if (++nStable == tasks.Count)
                        {
                            PrintExtensions.PrintDebug("Fixed-point reached after " + i);
                            return true;
                        }
                    }
                    else
                    {
                        task.Run();
                        nStable = 0;
                        if (TerminateTaskLoop)
                        {
                            PrintExtensions.PrintDebug("TerminateTaskLoop is set");
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}