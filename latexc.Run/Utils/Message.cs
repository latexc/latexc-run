﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.IO;

namespace LaTeXc.Run.Utils
{
    public enum MessageType : byte
    {
        Info,
        Warning,
        Error
    }

    public struct Message
    {
        public MessageType Type { get; }
        public string FileName { get; }
        public int? LineNo { get; }
        public string Content { get; }

        public Message(MessageType type, string fileName, int? lineNo, string content)
        {
            Type = type;
            FileName = fileName;
            LineNo = lineNo;
            Content = content;
        }

        public void Emit()
        {
            var writer = Type switch
            {
                MessageType.Info => PrintExtensions.STDOUT,
                MessageType.Warning => PrintExtensions.STDOUT,
                MessageType.Error => PrintExtensions.STDERR,
                _ => throw new ArgumentOutOfRangeException()
            };

            string fInfo;
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                fInfo = FileName.StartsWith("./") ? FileName.Substring(2) : FileName;
            }
            else
            {
                fInfo = "<no file>";
            }

            if (LineNo.HasValue)
            {
                fInfo += LineNo.Value;
            }

            fInfo += ": ";
            writer.Write(fInfo);

            switch (Type)
            {
                case MessageType.Info:
                    PrintExtensions.PrintInfo(Content);
                    break;
                case MessageType.Warning:
                    PrintExtensions.PrintWarning(Content);
                    break;
                case MessageType.Error:
                    PrintExtensions.PrintError(Content);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}