﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using LaTeXc.Run.Tasks;
using Newtonsoft.Json;

namespace LaTeXc.Run.Utils
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Database : IDisposable
    {
        private const string VERSION = "latexrun-db-v2";

        private string _filename;

        [JsonConstructor]
        private Database()
        {
        }

        public Database(string filename)
        {
            _filename = Path.GetFullPath(filename);

            // Make sure database directory exists
            var pathName = Path.GetDirectoryName(_filename);
            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            var lockPath = _filename + ".lock";
            if (File.Exists(lockPath))
            {
                if (!File.Exists(_filename))
                {
                    File.Delete(lockPath);
                }
                else
                {
                    //throw new ApplicationException("Database already in use");
                }
            }

            File.Create(lockPath);

            if (!File.Exists(_filename))
            {
                Version = VERSION;
            }
            else
            {
                try
                {
                    var db = JsonConvert.DeserializeObject<Database>(File.ReadAllText(_filename));

                    if (VERSION != db.Version)
                    {
                        throw new ApplicationException($"unknown database version {db.Version}");
                    }

                    Version = db.Version;
                    if (db.Tasks != null)
                    {
                        Tasks = db.Tasks;
                    }

                    if (db.Clean != null)
                    {
                        Clean = db.Clean;
                    }
                }
                catch (JsonException)
                {
                    throw new ApplicationException("file exists, but does not appear to be a LaTeXc Run database");
                }
            }
        }

        public HashCache HashCache { get; private set; } = new HashCache();

        [JsonProperty(Required = Required.Always)]
        public string Version { get; private set; }

        [JsonProperty(Required = Required.DisallowNull)]
        public Dictionary<string, TaskSummary> Tasks { get; private set; } = new Dictionary<string, TaskSummary>();

        [JsonProperty(Required = Required.DisallowNull)]
        public Dictionary<string, string> Clean { get; private set; } = new Dictionary<string, string>();

        public void Dispose()
        {
            FreeLock();
            GC.SuppressFinalize(this);
        }

        public void Commit()
        {
            var tmpFileName = _filename + ".tmp";
            if (File.Exists(tmpFileName))
            {
                File.Delete(tmpFileName);
            }

            using var fs = File.Create(tmpFileName);
            fs.Write(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(this)));
            fs.Flush();
            fs.Close();

            File.Delete(_filename);
            File.Move(tmpFileName, _filename);
        }

        public TaskSummary GetSummary(string taskId)
        {
            return Tasks.TryGetValue(taskId, out var value) ? value : null;
        }

        public void SetSummary(string taskId, TaskSummary summary)
        {
            Tasks[taskId] = summary;
        }

        public void AddClean(string fileName)
        {
            Clean[fileName] = HashCache.Get(fileName);
        }

        public void DoClean(string objDir)
        {
            var files = Clean;
            foreach (var (file, hash) in Clean)
            {
                var cacheHash = HashCache.Get(file);
                if (cacheHash == null) continue;
                if (hash == cacheHash)
                {
                    HashCache.Invalidate(file);
                }
            }

            Version = VERSION;
            Tasks.Clear();
            Clean.Clear();

            if (objDir != null)
            {
                Directory.Delete(objDir);
            }
        }

        private void FreeLock()
        {
            File.Delete(_filename + ".lock");
        }

        ~Database()
        {
            FreeLock();
        }
    }
}