﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace LaTeXc.Run.Utils
{
    public class KPathSearch
    {
        private readonly string _programName;

        public KPathSearch(string programName)
        {
            _programName = programName;
        }

        public string FindFile(string name, string format, string cwd = null)
        {
            var args = new[] {"kpsewhich", "-progname", _programName, "-format", format, name};

            var proc = new Process
            {
                EnableRaisingEvents = true,
                StartInfo =
                {
                    FileName = args[0],
                    Arguments = string.Join(' ', args).Substring(args[0].Length + 1),
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    WorkingDirectory = cwd ?? Environment.CurrentDirectory
                }
            };

            var output = new StringBuilder();

            proc.OutputDataReceived += (sender, e) => output.AppendLine(e.Data);

            proc.Start();
            proc.BeginOutputReadLine();
            proc.WaitForExit();

            if (proc.ExitCode > 1)
            {
                throw new Exception();
            }

            if (proc.ExitCode == 1)
            {
                return null;
            }

            var path = output.ToString().Trim();
            return cwd == null ? path : Path.Join(cwd, path);
        }
    }
}