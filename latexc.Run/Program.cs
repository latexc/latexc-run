﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using LaTeXc.Run.Utils;
using Mono.Options;

namespace LaTeXc.Run
{
    class Program
    {
        static int Main(string[] args)
        {
            var processName = Process.GetCurrentProcess().ProcessName;
            var stderr = Console.Error;
            var stdout = Console.Out;

            var showHelp = false;

            var parsedArgs = new Dictionary<string, object>();

            var opts = new OptionSet
            {
                $"Usage: {processName} [OPTIONS] FILE",
                "",
                "A 21st century LaTeX wrapper. Runs latex (and bibtex) the right number of times" +
                "so you don't have to, strips the log spew to make errors visible, and plays " +
                "well with standard build tools.",
                "",
                "Options:",
                {"o|output=", "Output file name (default: derived from input file)", v => parsedArgs[Runner.OUTPUT] = v},
                {"latex-cmd=", "LaTeX command (default: pdflatex)", v => parsedArgs[Runner.LATEX_CMD] = v},
                {"latex-args=", "Additional command-line arguments for latex.", v => parsedArgs[Runner.LATEX_ARGS] = v},
                {"bibtex-cmd=", "BibTeX command (default: bibtex)", v => parsedArgs[Runner.BIBTEX_CMD] = v},
                {"bibtex-args=", "Additional command-line arguments for bibtex", v => parsedArgs[Runner.BIBTEX_ARGS] = v},
                {"max-iterations=", "Max number of times to run latex before giving up (default: 10)", v => parsedArgs[Runner.MAX_ITERATIONS] = v},
                {
                    "W|nowarns=",
                    "Enable/disable warning from CLASS, which can be any package name, LaTeX warning class (e.g., font), bad box type (underfull, overfull, loose, tight), or 'all'",
                    v => parsedArgs[Runner.NOWARNS] = v?.Split(',')
                },
                {"O|obj-dir=", "Directory for intermediate files and control database (default: latex.out)", v => parsedArgs[Runner.OBJ_DIR] = v},
                {"debug", "Enable detailed debug output", v => parsedArgs["debug"] = v != null},
                {"verbose", "Enable EXTREMELY detailed debug output", v => parsedArgs["verbose"] = v != null},
                {"clean-all", "Delete output files", v => parsedArgs[Runner.CLEAN_ALL] = v != null},
                {"h|help", "Show this message and exit", v => showHelp = v != null},
            };

            List<string> extra;
            try
            {
                extra = opts.Parse(args);
            }
            catch (OptionException e)
            {
                stderr.WriteLine(e.Message);
                stderr.WriteLine($"Try `{processName} --help' for more information.");
                return 1;
            }

            if (showHelp)
            {
                opts.WriteOptionDescriptions(stdout);
                return 0;
            }

            string fileName;
            switch (extra.Count)
            {
                case 0:
                    stderr.WriteLine("You must specify the main latex document");
                    stderr.WriteLine($"Try `{processName} --help' for more information.");
                    return 1;
                case 1:
                    fileName = extra[0];
                    break;
                default:
                    stderr.WriteLine("You can only specify one latex document");
                    stderr.WriteLine($"Try `{processName} --help' for more information.");
                    return 1;
            }

            parsedArgs[Runner.FILE] = fileName;

            var result = Runner.Main(parsedArgs, stdout, stderr);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            return result;
        }
    }
}