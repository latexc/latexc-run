﻿using System.Linq;
using Newtonsoft.Json;

namespace LaTeXc.Run.Tasks
{
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class Dependency
    {
        [JsonConverter(typeof(StaticFunctionConverter))]
        [JsonProperty]
        public Task.InputFun InputFunction { get; private set; }

        [JsonProperty] public object[] Arguments { get; private set; }
        [JsonProperty] public object LastValue { get; private set; }

        public void Deconstruct(out Task.InputFun fn, out object[] args, out object val)
        {
            fn = InputFunction;
            args = Arguments;
            val = LastValue;
        }

        public Dependency(Task.InputFun inputFunction, object[] arguments, object lastValue)
        {
            InputFunction = inputFunction;
            Arguments = arguments;
            LastValue = lastValue;
        }

#pragma warning disable 659
        public override bool Equals(object obj)
        {
            return obj is Dependency other && Equals(other);
        }

        private bool Equals(Dependency other)
        {
            var (fn, args, val) = other;
            return Equals(InputFunction.Method.DeclaringType?.FullName + "@" + InputFunction.Method.Name, fn.Method.DeclaringType?.FullName + "@" + fn.Method.Name)
                   && Arguments.SequenceEqual(args)
                   && Equals(LastValue, val);
        }
    }
}