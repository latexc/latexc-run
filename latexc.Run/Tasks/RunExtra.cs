﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LaTeXc.Run.Tasks
{
    public abstract class RunExtra
    {
        protected RunExtra(int status)
        {
            Status = status;
        }

        public int Status { get; }
    }

    public class LatexRunExtra : RunExtra
    {
        public LatexRunExtra(string jobName, string outName, int status) : base(status)
        {
            JobName = jobName;
            OutName = outName;
        }

        public string JobName { get; }
        public string OutName { get; }
    }

    public class BibTeXRunExtra : RunExtra
    {
        public BibTeXRunExtra(HashSet<string> inputs, string outBase, int status) : base(status)
        {
            Inputs = inputs;
            OutBase = outBase;
        }

        public HashSet<string> Inputs { get; }
        public string OutBase { get; }
    }

    public class RunExtraJsonConverter : JsonConverter<RunExtra>
    {
        public override void WriteJson(JsonWriter writer, RunExtra value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }

        public override RunExtra ReadJson(JsonReader reader, Type objectType, RunExtra existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            var x = (JObject) serializer.Deserialize(reader);
            if (x == null)
            {
                return null;
            }

            if (x.ContainsKey("JobName"))
            {
                return x.ToObject<LatexRunExtra>();
            }
            else
            {
                return x.ToObject<BibTeXRunExtra>();
            }
        }
    }
}