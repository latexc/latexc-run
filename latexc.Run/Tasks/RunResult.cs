﻿using System.Collections.Generic;

namespace LaTeXc.Run.Tasks
{
    public class RunResult
    {
        public RunResult(List<string> outputFiles, RunExtra extra)
        {
            OutputFiles = outputFiles;
            Extra = extra;
        }

        public List<string> OutputFiles { get; }
        public RunExtra Extra { get; }
    }
}