﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using LaTeXc.Run.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LaTeXc.Run.Tasks
{
    [JsonObject(MemberSerialization.OptIn)]
    public abstract class Task
    {
        protected Task(Database db, string taskId)
        {
            Db = db;
            TaskId = taskId;
        }

        public Database Db { get; }
        [JsonProperty] public string TaskId { get; }

        [JsonProperty] private List<Dependency> Dependencies { get; } = new List<Dependency>();

        public virtual bool Stable
        {
            get
            {
                this.PrintVerbose($"Property '{nameof(Stable)}'");
                var lastSummary = Db.GetSummary(TaskId);
                var changed = lastSummary == null ? "never run" : SummaryChanged(lastSummary);
                if (string.IsNullOrEmpty(changed))
                {
                    this.PrintDebug($"{TaskId} stable");
                    return true;
                }
                else
                {
                    this.PrintDebug($"{TaskId} unstable (changed: {changed})");
                    return false;
                }
            }
        }

        protected RunExtra ResultExtra => Db.GetSummary(TaskId)?.Extra;

        public static string NormalizeInputPath(string path)
        {
            var head = Path.GetDirectoryName(path);
            var tail = Path.GetFileName(path);
            return Path.Join(head, tail);
        }

        protected abstract RunResult Execute();

        private string SummaryChanged(TaskSummary summary)
        {
            foreach (var (fn, args, val) in summary.Deps)
            {
                if (fn.Method.Name == nameof(InputUnstable))
                {
                    return nameof(InputUnstable);
                }

                var retval = fn(this, args);
                var equal = retval is IEnumerable en
                    ? en.Cast<object>().SequenceEqual(((IEnumerable) val).Cast<object>())
                    : Equals(retval, val);
                if (!equal)
                {
                    return $"{fn.Method.Name}({string.Join(", ", args)})";
                }
            }

            return null;
        }

        private TaskSummary MakeSummary(List<Dependency> deps, RunResult result)
        {
            return new TaskSummary(deps, result.OutputFiles, result.Extra);
        }

        protected T Input<T>(InputFun fun, params object[] args)
        {
            return (T) Input(fun, args);
        }

        protected object Input(InputFun fun, params object[] args)
        {
            var retval = fun(this, args);
            if (!Dependencies.Any(d => d.InputFunction == fun && d.Arguments.SequenceEqual(args) && d.LastValue == retval))
            {
                Dependencies.Add(new Dependency(fun, args, retval));
            }

            return retval;
        }

        public virtual int Report()
        {
            return 0;
        }

        public void Run()
        {
            var hashCache = Db.HashCache;
            var lastSummary = Db.GetSummary(TaskId);
            if (lastSummary != null)
            {
                foreach (var ioFilename in lastSummary.OutputFiles)
                {
                    this.PrintDebug($"pre-hashing {ioFilename}");
                    hashCache.Get(ioFilename);
                }
            }

            this.PrintDebug("running");

            Dependencies.Clear();
            var result = Execute();

            foreach (var file in result.OutputFiles)
            {
                hashCache.Invalidate(file);
            }

            foreach (var file in result.OutputFiles)
            {
                Input(InputFile, file);
            }

            Db.SetSummary(TaskId, MakeSummary(Dependencies, result));
            Dependencies.Clear();

            foreach (var file in result.OutputFiles)
            {
                Db.AddClean(file);
            }

            Db.Commit();
        }

        protected static object InputEnv(Task _, params object[] args)
        {
            return EnvHelper.Get((string) args[0]);
        }

        protected static object InputFile(Task self, params object[] args)
        {
            return self.Db.HashCache.Get((string) args[0]);
        }

        protected object InputUnstable(Task self, params object[] _)
        {
            return null;
        }

        public delegate object InputFun(Task self, params object[] args);
    }

    internal class StaticFunctionConverter : JsonConverter<Task.InputFun>
    {
        public override void WriteJson(JsonWriter writer, Task.InputFun value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value.Method.DeclaringType?.FullName + "@" + value.Method.Name);
        }

        public override Task.InputFun ReadJson(JsonReader reader, Type objectType, Task.InputFun existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            if (hasExistingValue)
            {
                return existingValue;
            }

            var signature = serializer.Deserialize<string>(reader);
            var split = signature.Split("@", 2);
            var type = Type.GetType(split[0]);
            var method = type?.GetMethod(split[1], BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
            return (Task.InputFun) method?.CreateDelegate(typeof(Task.InputFun));
        }
    }
}