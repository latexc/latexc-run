﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace LaTeXc.Run.Tasks
{
    public class TaskSummary
    {
        public TaskSummary(List<Dependency> deps, List<string> outputFiles, RunExtra extra)
        {
            Deps = new List<Dependency>(deps);
            OutputFiles = outputFiles.ToList();
            Extra = extra;
        }

        public List<Dependency> Deps { get; }
        public List<string> OutputFiles { get; }

        [JsonConverter(typeof(RunExtraJsonConverter))]
        public RunExtra Extra { get; }
    }
}