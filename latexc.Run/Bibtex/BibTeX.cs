﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using LaTeXc.Run.Latex;
using LaTeXc.Run.Tasks;
using LaTeXc.Run.Utils;

namespace LaTeXc.Run.BibTeX
{
    public class BibTeX : Task
    {
        public BibTeX(Database db, LaTeX taskLatex, string bibtexCmd, string[] bibtexArgs, string[] noWarns, string objDir)
            : base(db, "bibtex::" + NormalizeInputPath(taskLatex.FileName))
        {
            LatexTask = taskLatex;
            Cmd = bibtexCmd;
            CmdArgs = bibtexArgs;
            ObjDir = objDir;
        }

        private new BibTeXRunExtra ResultExtra => (BibTeXRunExtra) base.ResultExtra;

        public string ObjDir { get; set; }

        public string[] CmdArgs { get; set; }

        public string Cmd { get; set; }

        public LaTeX LatexTask { get; set; }

        public override bool Stable
        {
            get
            {
                this.PrintVerbose($"Property '{nameof(Stable)}'");
                var jobName = LatexTask.JobName;

                return jobName == null
                       || !File.Exists(jobName + ".aux")
                       || !FindBibCommands(Path.GetDirectoryName(jobName), jobName + ".aux")
                       || base.Stable;
            }
        }

        private bool IsBiber => Cmd.Contains("biber");

        private bool FindBibCommands(string baseDir, string auxName)
        {
            this.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(FindBibCommands)}'");
            return FindBibCommands(baseDir, auxName, new Stack<string>());
        }

        private bool FindBibCommands(string baseDir, string auxName, Stack<string> stack)
        {
            this.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(FindBibCommands)}'");
            if (stack.Contains(auxName))
            {
                throw new ApplicationException(".aux file loop");
            }

            stack.Push(auxName);

            if (!File.Exists(auxName))
            {
                this.PrintDebug("cannot find bib commands, aux does not exist");
                return false;
            }

            var auxData = File.ReadAllText(auxName);

            if (Regex.IsMatch(auxData, @"^\\bibstyle\{", RegexOptions.Multiline) ||
                Regex.IsMatch(auxData, @"^\\bibdata\{", RegexOptions.Multiline))
            {
                this.PrintDebug("found bibstyle or bibdata");
                return true;
            }

            if (Regex.IsMatch(auxData, @"^\\abx@aux@cite\{", RegexOptions.Multiline))
            {
                this.PrintDebug("found bibstyle or bibdata");
                return true;
            }

            var matches = Regex.Matches(auxData, @"^\\@input\{([^}]*)\}", RegexOptions.Multiline);
            foreach (Match m in matches)
            {
                if (FindBibCommands(baseDir, Path.Join(baseDir, m.Groups[1].Value), stack))
                {
                    return true;
                }
            }

            this.PrintDebug("didnt find bib commands");
            return false;
        }

        private static object InputArgs(Task self1, params object[] _)
        {
            var self = (BibTeX) self1;
            self.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(InputArgs)}'");
            var auxName = self.LatexTask.JobName;
            if (!self.IsBiber)
            {
                auxName += ".aux";
            }

            self.PrintVerbose($"Local auxName is '{auxName}'");

            var args = new List<string>(1 + self.CmdArgs.Length + 1);
            args.Add(self.Cmd);
            args.AddRange(self.CmdArgs);
            args.Add(auxName);

            self.PrintVerbose($"BibTeX command is '{string.Join(' ', args)}'");

            return args.ToArray();
        }

        private static object InputCwd(Task self1, params object[] _)
        {
            var self = (BibTeX) self1;
            self.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(InputCwd)}'");
            return self.LatexTask.Cwd;
        }

        private static object InputAuxFile(Task self1, params object[] args)
        {
            var self = (BibTeX) self1;
            self.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(InputAuxFile)}'");
            var auxName = (string) args[0];
            self.PrintDebug($"hashing filtered aux file {auxName}");
            if (File.Exists(auxName))
            {
                var h = new SHA256CryptoServiceProvider();
                var lines = File.ReadAllLines(auxName).Where(line =>
                    line.StartsWith("\\citation{") ||
                    line.StartsWith("\\bibdata{") ||
                    line.StartsWith("\\bibstyle{") ||
                    line.StartsWith("\\@input{") ||
                    line.StartsWith("\\abx@aux@cite{")).ToArray();
                return Convert.ToBase64String(h.ComputeHash(Encoding.UTF8.GetBytes(string.Join("\n", lines))));
            }

            self.PrintDebug($"{auxName} does not exist");
            return null;
        }

        private string PathJoin(string first, string rest)
        {
            this.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(PathJoin)}'");
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                return first + ";" + (rest ?? "");
            }

            return first + ":" + (rest ?? "");
        }

        protected override RunResult Execute()
        {
            this.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(Execute)}'");
            // This gets complicated when \include is involved.  \include
            // switches to a different aux file and records its path in the
            // main aux file.  However, BibTeX does not consider this path
            // to be relative to the location of the main aux file, so we
            // have to run BibTeX *in the output directory* for it to
            // follow these includes (there's no way to tell BibTeX other
            // locations to search).  Unfortunately, this means BibTeX will
            // no longer be able to find local bib or bst files, but so we
            // tell it where to look by setting BIBINPUTS and BSTINPUTS
            // (luckily we can control this search).  We have to pass this
            // same environment down to Kpathsea when we resolve the paths
            // in BibTeX's log.

            var args = Input<string[]>(InputArgs);
            var cwd = Input<string>(InputCwd);

            this.PrintDebug($"running '{string.Join(' ', args)}' in {cwd}");

            var proc = new Process
            {
                EnableRaisingEvents = true,
                StartInfo =
                {
                    FileName = args[0],
                    Arguments = string.Join(' ', args).Substring(args[0].Length + 1),
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    WorkingDirectory = cwd
                }
            };

            proc.StartInfo.EnvironmentVariables["BIBINPUTS"] = PathJoin(Environment.CurrentDirectory, EnvHelper.Get("BIBINPUTS"));
            proc.StartInfo.EnvironmentVariables["BSTINPUTS"] = PathJoin(Environment.CurrentDirectory, EnvHelper.Get("BSTINPUTS"));

            var logBuffer = new List<string>();


            proc.OutputDataReceived += (sender, e) => this.PrintDebug("BIBTEX OUTPUT: " + e.Data);
            proc.ErrorDataReceived += (sender, e) => this.PrintDebug("BIBTEX ERROR: " + e.Data);

            proc.OutputDataReceived += (sender, e) => logBuffer.Add(e.Data);

            proc.Start();
            proc.BeginOutputReadLine();
            proc.WaitForExit();

            var fullLog = string.Join('\n', logBuffer);

            ParseInputs(fullLog, out var inputs, out var auxNames, out var outBase, cwd);
            if (outBase == null)
            {
                outBase = LatexTask.JobName;
            }

            if (inputs.Count == 0 && auxNames.Count == 0)
            {
                // TODO: Output
                throw new ApplicationException("failed to execute bibtex task");
            }

            Input(InputEnv, "TEXMFOUTPUT");
            Input(InputEnv, "BSTINPUTS");
            Input(InputEnv, "BIBINPUTS");
            Input(InputEnv, "PATH");

            foreach (var path in auxNames)
            {
                Input(InputAuxFile, path);
            }

            foreach (var path in inputs)
            {
                Input(InputFile, path);
            }

            if (IsBiber)
            {
                outBase = Path.Join(cwd, outBase);
            }

            this.PrintVerbose($"BibTeX outBase is '{outBase}'");

            return new RunResult(new List<string>
            {
                outBase + ".bbl",
                outBase + ".blg"
            }, new BibTeXRunExtra(inputs, outBase, proc.ExitCode));
        }

        private void ParseInputs(string fullLog, out HashSet<string> inputs, out HashSet<string> auxNames, out string outBase, string cwd)
        {
            this.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(ParseInputs)}'");
            // BibTeX conveniently logs every file that it opens, and its
            // log is actually sensible (see calls to a_open_in in
            // bibtex.web.)  The only trick is that these file names are
            // pre-kpathsea lookup and may be relative to the directory we
            // ran BibTeX in.
            // 
            // Because BibTeX actually depends on very little in the .aux
            // file (and it's likely other things will change in the .aux
            // file), we don't count the whole .aux file as an input, but
            // instead depend only on the lines that matter to BibTeX.

            var kPath = new KPathSearch("bibtex");

            string FindFile(string fileName, string format)
            {
                return kPath.FindFile(fileName, format, cwd);
            }

            inputs = new HashSet<string>();
            auxNames = new HashSet<string>();
            outBase = null;

            foreach (var line in fullLog.Split('\n', '\r'))
            {
                var m = Regex.Match(line, "(?:The top-level auxiliary file:|A level-[0-9]+ auxiliary file:) (.*)");
                if (m.Success)
                {
                    auxNames.Append(Path.Join(cwd, m.Groups[1].Value));
                    continue;
                }

                m = Regex.Match(line, "(?:(The style file:)|(Database file #[0-9]+:)) (.*)");

                if (m.Success)
                {
                    var fileName = m.Groups[3].Value;
                    if (!string.IsNullOrEmpty(m.Groups[1].Value))
                    {
                        fileName = FindFile(fileName, "bst");
                    }
                    else if (!string.IsNullOrEmpty(m.Groups[2].Value))
                    {
                        fileName = FindFile(fileName, "bib");
                    }

                    if (Path.IsPathRooted(fileName))
                    {
                        var relName = Path.GetRelativePath(cwd, fileName);
                        if (!relName.Contains("../") && !relName.Contains("..\\"))
                        {
                            fileName = relName;
                        }
                    }

                    this.PrintVerbose($"fileName is '{fileName}'");
                    inputs.Add(fileName);
                }

                m = Regex.Match(line, "Found BibTeX data source '(.*?)'");
                if (m.Success)
                {
                    inputs.Add(m.Groups[1].Value);
                }

                m = Regex.Match(line, "Logfile is '(.*?)'");
                if (m.Success)
                {
                    outBase = m.Groups[1].Value.Substring(0, m.Groups[1].Value.Length - 4);
                }
            }
        }

        public override int Report()
        {
            this.PrintVerbose($"Method '{nameof(BibTeX)}::{nameof(Report)}'");
            var extra = ResultExtra;
            if (extra == null)
            {
                return 0;
            }

            var log = File.ReadAllText(ResultExtra.OutBase + ".blg");
            var inputs = ResultExtra.Inputs;

            var filter = new BibTeXFilter(log, inputs);
            foreach (var message in filter.Messages)
            {
                message.Emit();
            }

            if (extra.Status >= 2)
            {
                return 1;
            }

            return 0;
        }
    }
}