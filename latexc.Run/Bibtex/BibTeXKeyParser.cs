﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LaTeXc.Run.BibTeX
{
    public class BibTeXKeyParser
    {
        private static readonly Regex IdentRegex = new Regex("(?![0-9])([^\\x00-\\x20\\x80-\\xff \\t\"#%\'(),={}]+)[ \\t\\n]*");
        private static readonly Regex AtRegex = new Regex("[^@]*@[ \\t\\n]*");
        private static readonly Regex BracketRegex = new Regex("([{(])[ \\t\\n]*");
        private static readonly Regex BracketRegexCurled = new Regex("([^, \\t\\n}]*)");
        private static readonly Regex BracketRegexNormal = new Regex("([^, \\t\\n]*)");

        private int _pos;
        private string _data;
        private Match _match;

        public IEnumerable<(string, int)> Parse(string data)
        {
            _pos = 0;
            _data = data;
            // Find the next entry
            while (Consume(AtRegex).Success)
            {
                // What entry?
                if (!Consume(IdentRegex).Success)
                {
                    continue;
                }

                var type = _match.Groups[1].Value;
                if (type == "comment")
                {
                    continue;
                }

                var start = _pos;
                if (!Consume(BracketRegex).Success)
                {
                    continue;
                }

                string closing;
                Regex keyRegex;
                switch (_match.Groups[1].Value)
                {
                    case "{":
                        closing = "}";
                        keyRegex = BracketRegexCurled;
                        break;
                    case "(":
                        closing = ")";
                        keyRegex = BracketRegexNormal;
                        break;
                    default: throw new NotSupportedException();
                }

                if (type != "preamble" && type != "string")
                {
                    // Regular entry, get the key
                    if (Consume(keyRegex).Success)
                    {
                        yield return (_match.Groups[1].Value, LineNo());
                    }
                }

                // Consume body
                _pos = start;
                Balanced(closing);
            }
        }

        private void Balanced(string closing)
        {
            _pos++;
            var level = 0;
            var skip = new Regex("[{}" + closing + "]");
            while (true)
            {
                var match = skip.Match(_data, _pos);
                if (!match.Success)
                {
                    break;
                }

                _pos = match.Index + match.Length;
                var ch = match.Groups[0].Value;
                if (level == 0 && ch == closing)
                {
                    break;
                }
                else if (ch == "{")
                {
                    level++;
                }
                else if (ch == "}")
                {
                    level--;
                }
            }
        }

        private Match Consume(Regex regex)
        {
            _match = regex.Match(_data, _pos);
            if (_match.Success)
            {
                _pos = _match.Index + _match.Length;
            }

            return _match;
        }

        private int LineNo()
        {
            return _data.Take(_pos).Count(c => c == '\n') + 1;
        }
    }
}