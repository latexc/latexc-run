﻿// --------------------------------------------------------------------------------
// Copyright (c) 2020 Philipp Matthaeus (https://www.phimath.de)
//
// This file is licenced under the GNU General Public Licence Version 3.
// You can view the full licence at https://www.gnu.org/licenses/gpl-3.0.en.html.
// --------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using LaTeXc.Run.Utils;

namespace LaTeXc.Run.BibTeX
{
    public class BibTeXFilter
    {
        private readonly HashSet<string> _inputs;
        private Dictionary<string, (string, int)> _keyLocs;

        public BibTeXFilter(string data, HashSet<string> inputs)
        {
            _inputs = inputs;
            Messages = new List<Message>();
            _keyLocs = null;

            var prevLine = "";
            foreach (var line in data.Split('\n', '\r', StringSplitOptions.RemoveEmptyEntries))
            {
                var msg = ProcessLine(prevLine, line);
                if (msg.HasValue)
                {
                    Messages.Add(msg.Value);
                }

                prevLine = line;
            }

            Messages.Sort((m1, m2) =>
            {
                var comp = string.Compare(m1.FileName ?? "", m2.FileName ?? "", StringComparison.Ordinal);
                if (comp == 0)
                {
                    comp = (m1.LineNo ?? 0).CompareTo(m2.LineNo ?? 0);
                }

                return comp;
            });
        }

        private Message? ProcessLine(string prevLine, string line)
        {
            Match m = null;

            Match Match(string r)
            {
                m = Regex.Match(line, r);
                return m;
            }

            // BibTeX has many error paths, but luckily the set is closed,
            // so we can find all of them.  This first case is the
            // workhorse format.
            //
            // AUX errors: aux_err/aux_err_return/aux_err_print
            //
            // BST errors: bst_ln_num_print/bst_err/
            // bst_err_print_and_look_for_blank_line_return/
            // bst_warn_print/bst_warn/
            // skip_token/skip_token_print/
            // bst_ext_warn/bst_ext_warn_print/
            // bst_ex_warn/bst_ex_warn_print/
            // bst_mild_ex_warn/bst_mild_ex_warn_print/
            // bst_string_size_exceeded
            //
            // BIB errors: bib_ln_num_print/
            // bib_err_print/bib_err/
            // bib_warn_print/bib_warn/
            // bib_one_of_two_expected_err/macro_name_warning/
            if (Match("(.*?)---?line ([0-9]+) of file (.*)").Success)
            {
                string text;
                var groupVal = m.Groups[1].Value;
                // Sometimes the real error is printed on the previous line
                if (groupVal == "while executing")
                {
                    // bst_ex_warn.  The real message is on the previous line
                    text = prevLine;
                }
                else
                {
                    text = string.IsNullOrEmpty(groupVal) ? prevLine : groupVal;
                }

                var (type, msg) = Canonicalize(text);
                return new Message(type, m.Groups[3].Value, int.Parse(m.Groups[2].Value), msg);
            }

            // overflow/print_overflow
            if (Match("Sorry---you\'ve exceeded BibTeX\'s (.*)").Success)
            {
                return new Message(MessageType.Error, null, null, "capacity exceeded: " + m.Groups[1].Value);
            }

            // confusion/print_confusion
            if (Match("(.*)---this can\'t happen$").Success)
            {
                return new Message(MessageType.Error, null, null, "internal error: " + m.Groups[1].Value);
            }

            // aux_end_err
            if (Match("I found (no .*)---while reading file (.*)").Success)
            {
                return new Message(MessageType.Error, m.Groups[2].Value, null, m.Groups[1].Value);
            }

            // bad_cross_reference_print/
            // nullxistent_cross_reference_error/
            // @<Complain about a nested cross reference@>
            //
            // This is split across two lines.  Match the second.
            if (Match("^refers to entry '").Success)
            {
                var (type, msg) = Canonicalize(prevLine + " " + line);
                msg = Regex.Replace("^a (bad cross reference)", "\\1", msg);
                // Try to give this key a location
                var m2 = Regex.Match(prevLine, "--entry \"[^\"]\"");
                if (m2.Success)
                {
                    var (fileName, lineNo) = FindKey(m2.Groups[1].Value);
                    return new Message(type, fileName, lineNo, msg);
                }
            }

            // print_missing_entry
            if (Match("Warning--I didn\'t find a database entry for (\". * \")").Success)
            {
                return new Message(MessageType.Warning, null, null, "no database entry for " + m.Groups[1].Value);
            }

            // x_warning
            if (Match("Warning--(.*)").Success)
            {
                // Most formats give warnings about "something in <key>".
                // Try to match it up.
                string fileName = null;
                int? lineNo = null;
                foreach (var m2 in Regex.Matches(" in ([^, \t\n]+)\b", line).Reverse())
                {
                    if (m2.Success)
                    {
                        (fileName, lineNo) = FindKey(m2.Groups[1].Value);
                        if (string.IsNullOrWhiteSpace(fileName))
                        {
                            break;
                        }
                    }
                }

                return new Message(MessageType.Warning, fileName, lineNo, m.Groups[1].Value);
            }

            // @<Clean up and leave@>
            if (Match("Aborted at line ([0-9]+) of file (.*)").Success)
            {
                return new Message(MessageType.Info, m.Groups[2].Value, int.Parse(m.Groups[1].Value), "aborted");
            }

            // biber type errors
            if (Match("^.*> WARN - (.*)$").Success)
            {
                PrintExtensions.STDERR.WriteLine("[ WARN  ] " + m.Groups[1].Value);
                var m2 = Regex.Match(m.Groups[1].Value, "(.*) in file '(. * ?)', skipping ...");
                if (m2.Success)
                {
                    return new Message(MessageType.Warning, m2.Groups[2].Value, 0, m2.Groups[1].Value);
                }

                return new Message(MessageType.Warning, null, null, m.Groups[1].Value);
            }


            if (Match("^.*> ERROR - (.*)$").Success)
            {
                var m2 = Regex.Match(m.Groups[1].Value, @"BibTeX subsystem: (.*?), line (\d+), (.*)$");
                if (m2.Success)
                {
                    return new Message(MessageType.Error, m2.Groups[1].Value, int.Parse(m2.Groups[2].Value), m2.Groups[3].Value);
                }

                return new Message(MessageType.Error, null, null, m.Groups[1].Value);
            }

            return null;
        }

        private (string, int?) FindKey(string key)
        {
            if (_keyLocs == null)
            {
                var p = new BibTeXKeyParser();
                _keyLocs = new Dictionary<string, (string, int)>();
                foreach (var file in _inputs)
                {
                    var data = File.ReadAllText(file);
                    foreach (var (pKey, lineNo) in p.Parse(data))
                    {
                        _keyLocs[pKey] = (file, lineNo);
                    }
                }
            }

            return _keyLocs.TryGetValue(key, out var tuple) ? tuple : ((string) null, (int?) null);
        }

        private (MessageType, string) Canonicalize(string msg)
        {
            MessageType type;
            if (msg.StartsWith("Warning"))
            {
                msg = Regex.Replace(msg, "^Warning-", "");
                type = MessageType.Warning;
            }
            else
            {
                type = MessageType.Error;
            }

            msg = Regex.Replace(msg, @"^I(\'m| was)?", "");
            msg = msg.Substring(0, 1).ToLower() + msg.Substring(1);
            return (type, msg);
        }

        public List<Message> Messages { get; private set; }
    }
}