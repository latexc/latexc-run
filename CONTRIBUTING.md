If you want to contribute, message me at phi@phimath.de and we can discuss
further steps.

At any time, you can of course create merge requests and put in your changes.
Please note that you must comply with my code style (which should be pretty
straight forward) and naming conventions (which e.g. JetBrains Rider can
automatically detect from the existing code) to qualify for a merge.
If your code does not comply, we sure will find a way of ensuring compliance
in a merge commit.