# LaTeXc Run

This project is an intelligent wrapper for the latex and bibtex commands. 
Based on .NET Core 3.1, it can be run on any platform supprted by the runtime 
(and of course LaTeX!).

Note that this project is a port of the Python script latexrun, which you can 
find at https://github.com/aclements/latexrun.

Like latexrun, LaTeXc Run is not a build system. It will not convert graphics 
in the right format, monitor for file changes or start your local PDF previewer.

## Features

* Runs latex the right number of times. LaTeX's iterative approach is  a poor 
  match for build tools that expect to run a task once and be done with it. 
  LaTeXc Run hides this complexity by running LaTeX (and BibTeX) as many times 
  as necessary and no more. Only the results from the final run are shown, 
  making it act like a standard, single-run build task.

* Surfaces error messages and warnings. LaTeX and related tools bury errors and 
  useful warnings in vast wastelands of output noise. LaTeXc Run prints only the 
  messages that matter, in a format understood by modern tools. 
  LaTeXc Run even figures out file names and line numbers for many BibTeX errors 
  that usually don't indicate their source.

        paper.tex:140: Overfull \hbox (15pt too wide)
        paper.tex:400: Reference `sec:eval' on page 5 undefined
        local.bib:230: empty booktitle in clements:commutativity

* Cleaning.  LaTeX's output files are legion. LaTeXc Run keeps track of them and 
  can clean them up for you.

* Atomic commit.  LaTeXc Run updates output files atomically, which means your 
  PDF reader will no longer crash or complain about broken xref tables when it 
  catches latex in the middle of writing out a PDF.

* Easy {.git,.hg,svn:}ignore.  Just ignore `latex.out/`.  Done!

Integrating with make
=====================

LaTeXc Run does its own dependency tracking (at a finer granularity than `make`).  
Since it also does nothing if no dependencies have changed, it's easy to 
integrate with `make` using phony targets.  Here's a complete example:

```Makefile
.PHONY: FORCE
paper.pdf: FORCE {files that need to be generated, if any}
	LaTeXc.Run paper.tex

.PHONY: clean
clean:
	LaTeXc.Run --clean-all
```

Note that `paper.pdf` depends on a phony target, but is not itself phony, since 
this would cause `make` to consider anything that *depended* on `paper.pdf` to 
always be out of date.  Instead, `make` only considers targets that depend on 
`paper.pdf` out of date if LaTeXc Run actually updates `paper.pdf`.



How LaTeXc Run works
==================

LaTeXc Run views the world as a cyclic graph of deterministic functions of 
system state:

         .tex → ┌───────┐ ─────────────────────────→ .pdf
    ╭─── .aux → │       │ → .aux ──╮
    │╭── .toc → │ latex │ → .toc ─╮│
    ││╭─ .bbl → │       │ → .log  ││
    │││     … → └───────┘ → …     ││
    │╰────────────────────────────╯│
    ╰──────────────────────────────┤
      │                            ↓
      │                 .bst → ┌────────┐ → .blg
      │                 .bib → │ bibtex │ → .bbl ─╮
      │                        └────────┘         │
      ╰───────────────────────────────────────────╯

LaTeXc Run's goal is to find the *fixed-point* of this computation. Put simply, 
it runs latex and bibtex repeatedly until it can guarantee that additional runs 
will not change the output.

A direct approach could be to check the output of latex and bibtex after each 
run and stop when the output is the same from one run to the next. This works, 
but runs latex too many times; it will have already produced its final output 
one iteration before this approach can figure out that it's done.

Instead, LaTeXc Run models latex and bibtex as deterministic functions of the 
file system state, their command-line arguments, and certain environment 
variables.  Hence, if the *inputs* to latex and bibtex do not change over one 
iteration, then LaTeXc Run can guarantee that the output also will not change, 
and stop.


Known issues
============

When a missing file is silently ignored by latex (e.g., `\openin`,
`\IfFileExists`), LaTeXc Run can't track that the *lack* of that file
was important.  Hence, if a missing file is later created, LaTeXc Run
may not re-run latex.  However, missing files that cause latex to
abort (e.g., missing files in `\input`) as well as missing `\include`
files (which do not cause aborts) are handled.

Command-line usage errors when calling latex appear before "This is
pdfTeX" and are not logged and therefore often not reported.


To do
=====

* Solve the problem of missing input files.  LaTeX doesn't record inputs that it 
  couldn't read, so we don't know about them even though they affect the 
  computation (often seriously!). Possible solutions include ptrace, FUSE, and 
  fanotify, but none of these are portable or easily accessible from Python.

* Provide a way to disable output filters for things that do their own output 
  parsing (e.g., AUCTeX).

* Integrate even better with `make`. Phony rules are okay, but will force `make` 
  dependencies to be generated even if LaTeXc Run ultimately does nothing. Since 
  LaTeXc Run's dependencies are finer-grained than `make`'s, it might be 
  necessary to shell out to LaTeXc Run to do this.

* Separate clean data by source file so you can clean a single input file's outputs.

* Some important things like undefined references are only considered warnings 
  since they don't stop compilation. Maybe distinguish document-mangling 
  warnings (almost but not quite errors) and format warnings (can be ignored 
  if justified).

* Perhaps separate out the LaTeX output filter so it can be used on its own as a 
  pipe filter. It could even be used on high-level output, like from make, by 
  acting as a pass-through until the "This is (pdf)TeX" line up to the 
  "Transcript written on" line (or anything else that can end TeX's log output).